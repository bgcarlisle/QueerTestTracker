<?php

include_once ("config.php");

if (submit_entry ($_POST['type'], $_POST['title'], $_POST['year'], $_POST['details'], $_POST['imdb_link'], $_POST['queer_character'], $_POST['named'], $_POST['name'], $_POST['non_deniable'], $_POST['blink_miss_it'], $_POST['tragic'], $_POST['cishet_comfort'], $_POST['notes'], $_POST['notes_contain_spoilers'], $_POST['submitter'])) {

    make_backup ();

    $success_notice = '<div class="alert alert-success" role="alert">Successfully submitted</div>';
} else {
    $success_notice = '<div class="alert alert-danger" role="alert">Error in submission</div>';
}

include ("index.php");

?>

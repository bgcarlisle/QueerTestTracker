# QueerTestTracker

A web app for keeping track of media that passes or fails The Queerbait-Tragicqueer-Cishet-comfort Test.[^1]

See a live version at [qt.bgcarlisle.com](https://qt.bgcarlisle.com "qt.bgcarlisle.com")

[^1]: [The Queerbait-Tragicqueer-Cishet-comfort Test](https://scholar.social/@bgcarlisle/103946789974966399 "The Queerbait-Tragicqueer-Cishet-comfort Test")

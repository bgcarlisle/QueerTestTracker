<?php

function get_front_page_table () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `media` WHERE `approved` = 1 ORDER BY `year` DESC, `title` ASC, `details`;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_number_full_marks () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT COUNT(*) as `n` FROM `media` WHERE `approved` = 1 AND `queer_character` = 1 AND `named` = 1 AND `non_deniable` = 1 AND `blink_miss_it` = 0 AND `tragic` = 0 AND `cishet_comfort` = 0;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result[0]['n'];
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_admin_page_table () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `media` ORDER BY `approved`, `year` DESC, `title` ASC, `details`;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function admin_save_new_entry ($type, $title, $year, $details, $imdb_link, $queer_character, $named, $name, $non_deniable, $blink_miss_it, $tragic, $cishet_comfort, $notes, $notes_contain_spoilers) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `media` (`type`, `title`, `year`, `details`, `imdb_link`, `queer_character`, `named`, `name`, `non_deniable`, `blink_miss_it`, `tragic`, `cishet_comfort`, `notes`, `notes_contain_spoilers`, `submitter`, `approved`, `when_approved`) VALUES (:type, :title, :year, :details, :imdb_link, :queer_character, :named, :name, :non_deniable, :blink_miss_it, :tragic, :cishet_comfort, :notes, :notes_contain_spoilers, 'admin', 1, NOW())");

	$stmt->bindParam(':type', $ty);
	$stmt->bindParam(':title', $ti);
	$stmt->bindParam(':year', $ye);
	$stmt->bindParam(':details', $de);
	$stmt->bindParam(':imdb_link', $im);
	$stmt->bindParam(':queer_character', $qc);
	$stmt->bindParam(':named', $nd);
	$stmt->bindParam(':name', $na);
	$stmt->bindParam(':non_deniable', $no);
	$stmt->bindParam(':blink_miss_it', $bl);
	$stmt->bindParam(':tragic', $tr);
	$stmt->bindParam(':cishet_comfort', $st);
	$stmt->bindParam(':notes', $nt);
	$stmt->bindParam(':notes_contain_spoilers', $ns);

	$ty = $type;
	$ti = $title;
	$ye = $year;

	if ( $details == "na" ) {
	    $de = NULL;
	} else {
	    $de = $details;
	}

	if ( $imdb_link == "" ) {
	    $im = NULL;
	} else {
	    $im = $imdb_link;
	}
	
	$qc = $queer_character;

	if ( $named == "na" ) {
	    $nd = NULL;
	} else {
	    $nd = $named;
	}

	if ( $name == "" ) {
	    $na = NULL;
	} else {
	    $na = $name;
	}

	if ( $non_deniable == "na" ) {
	    $no = NULL;
	} else {
	    $no = $non_deniable;
	}

	if ( $blink_miss_it == "na" ) {
	    $bl = NULL;
	} else {
	    $bl = $blink_miss_it;
	}

	if ( $tragic == "na" ) {
	    $tr = NULL;
	} else {
	    $tr = $tragic;
	}

	if ( $cishet_comfort == "na" ) {
	    $st = NULL;
	} else {
	    $st = $cishet_comfort;
	}

	if ( $notes == "" ) {
	    $nt = NULL;
	} else {
	    $nt = $notes;	    
	}
	
	if ( $notes_contain_spoilers == "na" ) {
	    $ns = NULL;
	} else {
	    $ns = $notes_contain_spoilers;
	}
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function admin_save_edit ($media_id, $type, $title, $year, $details, $imdb_link, $queer_character, $named, $name, $non_deniable, $blink_miss_it, $tragic, $cishet_comfort, $notes, $notes_contain_spoilers) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("UPDATE `media` SET `type`=:type, `title`=:title, `year`=:year, `details`=:details, `imdb_link`=:imdb_link, `queer_character`=:queer_character, `named`=:named, `name`=:name, `non_deniable`=:non_deniable, `blink_miss_it`=:blink_miss_it, `tragic`=:tragic, `cishet_comfort`=:cishet_comfort, `notes`=:notes, `notes_contain_spoilers`=:notes_contain_spoilers WHERE `id` = :mid");

	$stmt->bindParam(':type', $ty);
	$stmt->bindParam(':title', $ti);
	$stmt->bindParam(':year', $ye);
	$stmt->bindParam(':details', $de);
	$stmt->bindParam(':imdb_link', $im);
	$stmt->bindParam(':queer_character', $qc);
	$stmt->bindParam(':named', $nd);
	$stmt->bindParam(':name', $na);
	$stmt->bindParam(':non_deniable', $no);
	$stmt->bindParam(':blink_miss_it', $bl);
	$stmt->bindParam(':tragic', $tr);
	$stmt->bindParam(':cishet_comfort', $st);
	$stmt->bindParam(':notes', $nt);
	$stmt->bindParam(':notes_contain_spoilers', $ns);

	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;

	$ty = $type;
	$ti = $title;
	$ye = $year;

	if ( $details == "na" ) {
	    $de = NULL;
	} else {
	    $de = $details;
	}

	if ( $imdb_link == "" ) {
	    $im = NULL;
	} else {
	    $im = $imdb_link;
	}
	
	$qc = $queer_character;

	if ( $named == "na" ) {
	    $nd = NULL;
	} else {
	    $nd = $named;
	}

	if ( $name == "" ) {
	    $na = NULL;
	} else {
	    $na = $name;
	}

	if ( $non_deniable == "na" ) {
	    $no = NULL;
	} else {
	    $no = $non_deniable;
	}

	if ( $blink_miss_it == "na" ) {
	    $bl = NULL;
	} else {
	    $bl = $blink_miss_it;
	}

	if ( $tragic == "na" ) {
	    $tr = NULL;
	} else {
	    $tr = $tragic;
	}

	if ( $cishet_comfort == "na" ) {
	    $st = NULL;
	} else {
	    $st = $cishet_comfort;
	}

	if ( $notes == "" ) {
	    $nt = NULL;
	} else {
	    $nt = $notes;	    
	}
	
	if ( $notes_contain_spoilers == "na" ) {
	    $ns = NULL;
	} else {
	    $ns = $notes_contain_spoilers;
	}
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function submit_entry ($type, $title, $year, $details, $imdb_link, $queer_character, $named, $name, $non_deniable, $blink_miss_it, $tragic, $cishet_comfort, $notes, $notes_contain_spoilers, $submitter) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `media` (`type`, `title`, `year`, `details`, `imdb_link`, `queer_character`, `named`, `name`, `non_deniable`, `blink_miss_it`, `tragic`, `cishet_comfort`, `notes`, `notes_contain_spoilers`, `submitter`, `approved`) VALUES (:type, :title, :year, :details, :imdb_link, :queer_character, :named, :name, :non_deniable, :blink_miss_it, :tragic, :cishet_comfort, :notes, :notes_contain_spoilers, :submitter, 0)");

	$stmt->bindParam(':type', $ty);
	$stmt->bindParam(':title', $ti);
	$stmt->bindParam(':year', $ye);
	$stmt->bindParam(':details', $de);
	$stmt->bindParam(':imdb_link', $im);
	$stmt->bindParam(':queer_character', $qc);
	$stmt->bindParam(':named', $nd);
	$stmt->bindParam(':name', $na);
	$stmt->bindParam(':non_deniable', $no);
	$stmt->bindParam(':blink_miss_it', $bl);
	$stmt->bindParam(':tragic', $tr);
	$stmt->bindParam(':cishet_comfort', $st);
	$stmt->bindParam(':notes', $nt);
	$stmt->bindParam(':notes_contain_spoilers', $ns);
	$stmt->bindParam(':submitter', $su);

	$ty = $type;
	$ti = $title;
	$ye = $year;

	if ( $details == "na" ) {
	    $de = NULL;
	} else {
	    $de = $details;
	}

	if ( $imdb_link == "" ) {
	    $im = NULL;
	} else {
	    $im = $imdb_link;
	}
	
	$qc = $queer_character;

	if ( $named == "na" ) {
	    $nd = NULL;
	} else {
	    $nd = $named;
	}

	if ( $name == "" ) {
	    $na = NULL;
	} else {
	    $na = $name;
	}

	if ( $non_deniable == "na" ) {
	    $no = NULL;
	} else {
	    $no = $non_deniable;
	}

	if ( $blink_miss_it == "na" ) {
	    $bl = NULL;
	} else {
	    $bl = $blink_miss_it;
	}

	if ( $tragic == "na" ) {
	    $tr = NULL;
	} else {
	    $tr = $tragic;
	}

	if ( $cishet_comfort == "na" ) {
	    $st = NULL;
	} else {
	    $st = $cishet_comfort;
	}

	if ( $notes == "" ) {
	    $nt = NULL;
	} else {
	    $nt = $notes;	    
	}
	
	if ( $notes_contain_spoilers == "na" ) {
	    $ns = NULL;
	} else {
	    $ns = $notes_contain_spoilers;
	}

	if ($submitter == "") {
	    $su = NULL;
	} else {
	    $su = $submitter;
	}
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_media ( $media_id ) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `media` WHERE `id` = :mid LIMIT 1;");

	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_single_entry ( $media_id ) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `media` WHERE `id` = :mid LIMIT 1;");

	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result[0];
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function update_approved ($media_id) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `media` WHERE `id` = :mid;");

	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;

	$stmt->execute();

	$result = $stmt->fetchAll();

	$media = $result[0];
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($media['approved'] === NULL) {
	$new_app = 1;
    } else {
	switch ($media['approved']) {
	    case 0:
		$new_app = 1;
		break;
	    case 1:
		$new_app = 0;
		break;
	}
    }

    // Removes submitter information for privacy reasons
    
    if ($media['submitter'] == "admin") {
	$new_sub = "admin";
    } else {
	$new_sub = "external";
    }

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("UPDATE `media` SET `approved` = :newapp, `submitter` = :newsub WHERE `id` = :mid;");

	$stmt->bindParam(':newapp', $na);
	$stmt->bindParam(':newsub', $ns);
	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;
	$na = $new_app;
	$ns = $new_sub;
	
	if ($stmt->execute()) {

	    if ($new_app === NULL) {
		$display_app = "-";
	    } else {
		switch ($new_app) {
		    case 0:
			$display_app = "No";
			break;
		    case 1:
			$display_app = "Yes";
			break;
		}
	    }

	    try {

		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$stmt = $dbh->prepare("UPDATE `media` SET `when_approved` = NOW() WHERE `id` = :mid;");

		$stmt->bindParam(':mid', $mid);

		$mid = $media_id;

		$stmt->execute();
		
	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }

	    return $display_app;
	} else {
	    return "MySQL Error";
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function make_backup () {
    exec('mysqldump --user=' . DB_USER . ' --password=' . DB_PASS . ' --host=' . DB_HOST . ' ' . DB_NAME . ' > ' . '/home/bgc_qt/backups/' . date('Y-m-d-H-i') . '.sql' );
}

function export_tsv () {
    exec ( "mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT type, title, year, details, imdb_link, queer_character, named, name, non_deniable, blink_miss_it, tragic, cishet_comfort, REPLACE(REPLACE(notes, '\\\"', '&quot;'), CHAR(13,10), ' ') as notes, notes_contain_spoilers FROM media WHERE approved = 1 ORDER BY title, year, details;\" > " . ABS_PATH . "export.tsv" );
}

function suggest_correction ( $media_id, $suggestion, $submitter ) {
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `corrections` (`media_id`, `suggestion`, `submitter`) VALUES (:mid, :sug, :sub)");

	$stmt->bindParam(':mid', $mid);
	$stmt->bindParam(':sug', $sug);
	$stmt->bindParam(':sub', $sub);

	if ( $media_id != "na" ) {
	    $mid = $media_id;
	} else {
	    $mid = NULL;
	}

	$sug = $suggestion;
	$sub = $submitter;

	if ($stmt->execute()) {

	    if ( $media_id != "na" ) {
		
		$media = get_single_entry ($media_id);

		if ( $media['details'] != "" ) {
		    $details = " " . $media['details'];
		} else {
		    $details = "";
		}
		
		$subject = "Queer test: Suggested correction to " . $media['title'] . " (" . $media['year'] . ") " . $details;

		$body = "A suggested correction has been posted regarding " . $media['title'] . " (" . $media['year'] . ")" . $details . "\n\n";

		$body = $body . "DETAILS FOR ORIGINAL SUBMISSION\n\n";
		$body = $body . "Title: " . $media['title'] . "\n";
		$body = $body . "Year: " . $media['year'] . "\n";
		$body = $body . "Details: " . $media['details'] . "\n";
		$body = $body . "Link: " . $media['imdb_link'] . "\n";
		$body = $body . "Queer character: " . $media['queer_character'] . "\n";
		$body = $body . "Named: " . $media['named'] . "\n";
		$body = $body . "Name: " . $media['name'] . "\n";
		$body = $body . "Non-deniable: " . $media['non_deniable'] . "\n";
		$body = $body . "Blink-and-you-miss-it: " . $media['blink_miss_it'] . "\n";
		$body = $body . "Tragic: " . $media['tragic'] . "\n";
		$body = $body . "Written for cishet comfort: " . $media['cishet_comfort'] . "\n";
		$body = $body . "Notes: " . $media['notes'] . "\n";
		$body = $body . "Notes contain spoilers: " . $media['notes_contain_spoilers'] . "\n";
		$body = $body . "Original submitter: " . $media['submitter'] . "\n";
		$body = $body . "When approved: " . $media['when_approved'] . "\n\n";
		
	    } else {
		$subject = "Queer test: Suggested correction";

		$body = "A suggestion correction has been posted with no piece of media selected.\n\n";
	    }

	    $body = $body . "DETAILS FOR SUGGESTED CORRECTION\n\n";

	    $body = $body . "Suggested correction submitter: " . $submitter . "\n\nSuggestion: " . $suggestion;

	    $headers = array (
		'From' => ADMIN_EMAIL
	    );
	    
	    mail(
		ADMIN_EMAIL,
		$subject,
		$body,
		$headers
	    );
	    
	    return TRUE;
	} else {
	    return FALSE;
	}
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_corrections () {
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `corrections` ORDER BY `when_submitted` DESC;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_correction ($correction_id) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `corrections` WHERE `id` = :cid LIMIT 1;");

	$stmt->bindParam(':cid', $cid);

	$cid = $correction_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_old_corrections () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `corrections` WHERE `when_submitted` < NOW() - INTERVAL 3 DAY;");

	$stmt->bindParam(':cid', $cid);

	$cid = $correction_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_rss_items () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `rss` ORDER BY `when_posted` DESC;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function copy_to_rss ($media_id, $post_type = "new") {

    // Get the details from the media table
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `media` WHERE `id` = :mid LIMIT 1;");

	$stmt->bindParam(':mid', $mid);

	$mid = $media_id;

	$stmt->execute();

	$result = $stmt->fetchAll();

	$media = $result[0];
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    // Copy these details to the RSS table
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `rss` (`media_id`, `post_type`, `media_type`, `title`, `year`, `details`, `queer_character`, `named`, `non_deniable`, `blink_miss_it`, `tragic`, `cishet_comfort`) VALUES (:mid, :pt, :mt, :title, :year, :details, :qc, :named, :nd, :bmi, :tragic, :cc);");

	$stmt->bindParam(':mid', $medid);
	$stmt->bindParam(':pt', $pt);
	$stmt->bindParam(':mt', $mt);
	$stmt->bindParam(':title', $ti);
	$stmt->bindParam(':year', $ye);
	$stmt->bindParam(':details', $de);
	$stmt->bindParam(':qc', $qc);
	$stmt->bindParam(':named', $na);
	$stmt->bindParam(':nd', $nd);
	$stmt->bindParam(':bmi', $bm);
	$stmt->bindParam(':tragic', $tr);
	$stmt->bindParam(':cc', $cc);

	$medid = $media_id;
	$pt = $post_type;
	$mt = $media['type'];
	$ti = $media['title'];
	$ye = $media['year'];
	$de = $media['details'];
	$qc = $media['queer_character'];
	$na = $media['named'];
	$nd = $media['non_deniable'];
	$bm = $media['blink_miss_it'];
	$tr = $media['tragic'];
	$cc = $media['cishet_comfort'];
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_rss_item ($id) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `rss` WHERE `id` = :rid LIMIT 1;");

	$stmt->bindParam(':rid', $rid);

	$rid = $id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

?>

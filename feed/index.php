<?php

include_once ("../config.php");

header('Content-Type: application/rss+xml; charset=utf-8');

echo '<?xml version="1.0" encoding="UTF-8" ?>';

$items = get_rss_items ();

?>
<rss version="2.0">
    <channel>
	<title>The Queerbait-Tragicqueer-Cishet-comfort Test</title>
	<link><?php echo SITE_URL; ?></link>
	<description>Does this piece of media have at least one (1) named queer character who can't easily be mis-read as cishet, isn't a tragic queer, and hasn't been written primarily to make cishet people comfortable?</description>
	<?php foreach ( $items as $item ) { ?>
	    <item>
		<title><?php

		       switch ($item['post_type']) {
			   case "new":
			       echo $item['title'] . " (" . $item['year'] . ")";
			       break;
			   case "edit":
			       echo "Update to " . $item['title'] . " (" . $item['year'] . ")";
			       break;
		       }
		       
		       ?></title>
		<link><?php echo SITE_URL; ?>?s=<?php echo rawurlencode($item['title']); ?></link>
		<description>
		    <h1><?php

			switch ($item['post_type']) {
			    case "new":
				echo $item['title'] . " (" . $item['year'] . ")";
				break;
			    case "edit":
				echo $item['title'] . " (" . $item['year'] . ") - update";
				break;
			}
			
			?></h1>
		    
		    <?php
		    
		    if ($item['media_type'] != "") {
			echo "<p>" . $item['media_type'] . "</p>";
		    }

		    if ($item['details'] != "") {
			echo "<p>" . $item['details'] . "</p>";
		    }

		    ?>
		    
		    <?php if ($item['queer_character'] == 0) { ?>
			<p>This media contains no in-canon queer characters.</p>
			<p><a href="<?php echo SITE_URL; ?>?s=<?php echo rawurlencode($item['title']); ?>">View the entry on <?php echo SITE_URL; ?>.</a></p>
		    <?php } else { ?>
			<?php if ($item['named'] == 1 & $item['non_deniable'] == 1 & $item['blink_miss_it'] != 1 & $item['tragic'] != 1 & $item['cishet_comfort'] != 1 ) { ?>
			    <p>Full marks! This media contains at least one named character who is unambiguously queer, isn't a blink-and-you-miss-it queer, tragic or written for straight comfort!</p>
			<?php } else { // Less than "full marks" ?>
			    <?php if ($item['named'] == 1) { ?>
				<p>This media contains at least one named in-canon queer character.</p>
			    <?php } else { ?>
				<p>This media contains at least one in-canon queer character, but this character is not named.</p>
			    <?php } ?>
			    
			    <?php if ($item['non_deniable'] == 1) { ?>
				<p>This queer character would be very difficult to mis-read as cishet.</p>
			    <?php } else { ?>
				<p>This character is ambiguously queer and it would not be difficult to mis-read them as cishet.</p>
			    <?php } ?>
			    
			    <?php if ($item['blink_miss_it'] == 1) { ?>
				<p>The canonicity of this character being queer is based on a very small detail or a very short scene. This is "blink-and-you-miss-it" queer representation.</p>
			    <?php } else { ?>
				<p>The canonicity of this character being queer is not based on a very small detail or a very short scene. This is not "blink-and-you-miss-it" queer representation.</p>
			    <?php } ?>
			    
			    <?php if ($item['tragic'] == 1) { ?>
				<p>This is "tragic" queer representation. The queer character meets with a sad ending, or the story is primarily about their misfortunes.</p>
			    <?php } else { ?>
				<p>This is non-tragic queer representation.</p>
			    <?php } ?>

			    <?php if ($item['cishet_comfort'] == 1) { ?>
				<p>This queer character seems to have been written mainly to provide comfort to straight people, to coddle their delicate sensibilities or to make the point that queer people are or should be indistinguishable from cishet people.</p>
			    <?php } else { ?>
				<p>This queer character does not seem to have been written for the comfort of cishet people.</p>
			    <?php } ?>
			<?php } ?>
			<p>Names of characters and notes have been omitted to prevent spoilers. <a href="<?php echo SITE_URL; ?>?s=<?php echo rawurlencode($item['title']); ?>">View the entry on <?php echo SITE_URL; ?> for full details.</a></p>
		    <?php } ?>
		</description>
	    </item>
	<?php } ?>
    </channel>
</rss>


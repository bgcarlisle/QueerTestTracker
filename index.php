<?php

include_once ("config.php");

$rows = get_front_page_table ();

$full_marks_n = get_number_full_marks ();

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>The Queerbait-Tragicqueer-Cishet-comfort Test</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- QT css -->
	<link href="<?php echo SITE_URL; ?>qt.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<nav class="nav">
	    <a class="nav-link" href="#" onclick="show_citation(event);">Cite</a>
	    <a class="nav-link" href="https://codeberg.org/bgcarlisle/QueerTestTracker" target="_blank">Source</a>
	    <a class="nav-link" href="https://scholar.social/@bgcarlisle" target="_blank">Social media</a>
	    <a class="nav-link" href="<?php echo SITE_URL; ?>admin/">Admin</a>
	</nav>
	<div id="citeMask" class="hidden" onclick="hide_dialog (event);">&nbsp;</div>
	<div id="cite" class="notes">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>How to cite The Queerbait-Tragicqueer-Cishet-comfort Test</h2>
	    <h3>BibTeX</h3>
	    <pre>
@software{carlisle_qt_2020,
location = {{Retrieved from https://qt.bgcarlisle.com/}},
title = {The Queerbait-Tragicqueer-Cishet-comfort Test},
url = {https://qt.bgcarlisle.com/},
organization = {{The Grey Literature}},
date = {2020-04},
author = {Carlisle, Benjamin Gregory}
}
	    </pre>
	    <h3>Vancouver</h3>
	    <p>Carlisle BG. The Queerbait-Tragicqueer-Cishet-comfort Test [Internet]. Retrieved from https://qt.bgcarlisle.com/: The Grey Literature; 2020. Available from: https://qt.bgcarlisle.com/</p>
	    <h3>AMA</h3>
	    <p>Carlisle BG. <i>The Queerbait-Tragicqueer-Cishet-comfort Test</i>. Retrieved from https://qt.bgcarlisle.com/: The Grey Literature; 2020. https://qt.bgcarlisle.com/.</p>
            <h3>MLA</h3>
            <p>Carlisle, Benjamin Gregory. <i>The Queerbait-Tragicqueer-Cishet-comfort Test.</i> The Grey Literature, 2020, https://qt.bgcarlisle.com/.</p>
        </div>
	<?php
	
	foreach ( $rows as $note ) {

	    if ( ! is_null ( $note['notes'] ) ) { ?>
	    <div id="notes<?php echo $note['id']; ?>" class="notes">
		<button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close notes</button>

		<h2>Notes for <?php echo $note['title']; ?><?php if ( ! is_null ( $note['year'] ) ) { echo " (" . $note['year'] . ")"; } ?></h2>

		<?php if ( ! is_null ( $note['details'] ) ) {

		    echo '<p style="font-style: italic;">' . $note['details'] . '</p>';
		    
		}

		if ( $note['notes_contain_spoilers'] == 1) {

		    echo '<p><span class="badge badge-warning">These notes contain possible spoilers</span></p>';

		    echo '<p class="hidden" id="notes_content_' . $note['id'] . '">' . str_replace ("\n", "<br>", $note['notes']) . '</p>';
		    
		    echo '<button class="btn btn-primary btn-block" onclick="$(\'#notes_content_' . $note['id'] . '\').slideToggle();">Show/hide notes that may include spoilers</button>';

		} else {

		    echo '<p>' . str_replace ("\n", "<br>", $note['notes']) . '</p>';

		}?>


		
	    </div>
	<?php }
	
	}

	?>
	<div class="notes" id="submit">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>Submit a piece of media</h2>
	    <p>Please do not provide individual ratings of many related media where one summary rating will be equally informative. For example, I have included one entry for all the numbered episodes for Star Wars films, rather than a separate entry for each one.</p>
	    <p>Submissions will be reviewed for accuracy and informativeness before being posted.</p>
	    <form action="<?php echo SITE_URL; ?>submit.php" method="post">
		<div class="form-group">
		    <label for="type">Media type</label>
		    <input type="text" class="form-control" id="type" name="type" aria-describedby="textHelp">
		    <small id="textHelp" class="form-text text-muted">E.g. Film, Television series, Television programme</small>
		</div>
		<div class="form-group">
		    <label for="type">Title</label>
		    <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp">
		    <small id="titleHelp" class="form-text text-muted">E.g. Star Trek: Deep Space Nine</small>
		</div>
		<div class="form-group">
		    <label for="type">Year</label>
		    <input type="text" class="form-control" id="year" name="year" aria-describedby="yearHelp">
		    <small id="yearHelp" class="form-text text-muted">E.g. 1999</small>
		</div>
		<div class="form-group">
		    <label for="type">Details</label>
		    <input type="text" class="form-control" id="details" name="details" aria-describedby="detailsHelp">
		    <small id="detailsHelp" class="form-text text-muted">If it is an individual television programme, give the season, episode number and episode title, if any, in the following format: e.g. S07E14, "Chimera". If all the entries in a film series can be covered with a single entry, do so, e.g. for Star Wars, write "All the numbered episodes".</small>
		</div>
		<div class="form-group">
		    <label for="type">Link</label>
		    <input type="text" class="form-control" id="imdb_link" name="imdb_link" aria-describedby="imdbHelp">
		    <small id="imdbHelp" class="form-text text-muted">Provide a link to the IMDB or Wikipedia entry that corresponds to the media in question.</small>
		</div>

		<div class="form-group">
		    <label for="queer_character">Is there at least one queer character?</label>
		    <select class="form-control" id="queer_character" name="queer_character">
			<option value="na"></option>
			<option value="1">At least one queer character</option>
			<option value="0">No queer characters</option>
		    </select>
		    <small id="queer_characterHelp" class="form-text text-muted">Do not include fan-theories or details disclosed outside the media itself. Do not include campy or kinky cishet people. Only include in-canon, non-metaphorical queer representation.</small>
		</div>

		<div id="queer_character_questions">

		    <div class="form-group">
			<label for="named">Does this queer character have a name?</label>
			<select class="form-control" id="named" name="named">
			    <option value="na"></option>
			    <option value="1">The queer character has a name</option>
			    <option value="0">The queer character is not named</option>
			</select>
			<small id="namedHelp" class="form-text text-muted">The name of the queer character that passes or comes closest to passing all the requirements of The Queerbait-Tragicqueer-Cishet-comfort Test. In the case that there are many queer characters who would score the same, the first in alphabetical order.</small>
		    </div>

		    <div class="form-group">
			<label for="name">What is the name of the queer character?</label>
			<input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp">
			<small id="nameHelp" class="form-text text-muted">E.g. Odo</small>
		    </div>

		    <div class="form-group">
			<label for="non_deniable">Is the character clearly queer?</label>
			<select class="form-control" id="non_deniable" name="non_deniable">
			    <option value="na"></option>
			    <option value="1">Yes, this character is clearly queer</option>
			    <option value="0">No, this character is ambiguously queer</option>
			</select>
			<small id="non_deniableHelp" class="form-text text-muted">Would mis-reading this queer character as cishet be very difficult for even a naive cishet person, or one who is very determined to refuse to believe a character is queer? Of course it is impossible to write a queer character in such a way that the most determined cishet person couldn't refuse to acknowledge it.</small>
		    </div>
		    
		    <div class="form-group">
			<label for="blink_miss_it">Is this blink-and-you-miss-it "representation"?</label>
			<select class="form-control" id="blink_miss_it" name="blink_miss_it">
			    <option value="na"></option>
			    <option value="1">Blink-and-you-miss-it</option>
			    <option value="0">Non-blink-and-you-miss-it</option>
			</select>
			<small id="blink_miss_itHelp" class="form-text text-muted">If the canonicity of a character's queerness depends entirely on a very small detail or a very short scene, it is blink-and-you-miss-it queer representation.</small>
		    </div>

		    <div class="form-group">
			<label for="tragic">Is this tragic queer representation?</label>
			<select class="form-control" id="tragic" name="tragic">
			    <option value="na"></option>
			    <option value="1">Tragic</option>
			    <option value="0">Non-tragic</option>
			</select>
			<small id="tragicHelp" class="form-text text-muted">Does the queer character in question meet with a sad ending, or is their story primarily about their misfortunes? This includes, but is not limited to "bury your gays" media, where the queer character is killed.</small>
		    </div>

		    <div class="form-group">
			<label for="cishet_comfort">Is this queer character mainly written for cishet comfort?</label>
			<select class="form-control" id="cishet_comfort" name="cishet_comfort">
			    <option value="na"></option>
			    <option value="1">Written for cishet comfort</option>
			    <option value="0">Not written for cishet comfort</option>
			</select>
			<small id="cishet_comfortHelp" class="form-text text-muted">Does this character seem to have been written mainly to coddle the sensibilities of cishet people, or to make the point that queer people are, or should be, indistinguishable from cishet people? <a href="https://blog.bgcarlisle.com/2020/04/15/risk-that-a-queer-character-was-written-for-straight-comfort-assessment-tool/" target="_blank">If unclear, use this tool.</a></small>
		    </div>

		</div>

		<div class="form-group">
		    <label for="notes">Notes</label>
		    <textarea class="form-control" id="notes" name="notes" rows="5"></textarea>
		    <small id="notesHelp" class="form-text text-muted">Include anything that will provide context to the decisions made, but try to keep it short.</small>
		</div>

		<div class="form-group">
		    <label for="notes_contain_spoilers">Do these notes contain spoilers?</label>
		    <select class="form-control" id="notes_contain_spoilers" name="notes_contain_spoilers">
			<option value="na"></option>
			<option value="1">Yes, spoilers</option>
			<option value="0">No spoilers</option>
		    </select>
		    <small id="notesHelp" class="form-text text-muted">If a prospective reader might be surprised by the contents of the notes entered above, please indicate this.</small>
		</div>

		<div class="form-group">
		    <label for="type">Your email</label>
		    <input type="text" class="form-control" id="submitter" name="submitter" aria-describedby="submitterHelp">
		    <small id="submitterHelp" class="form-text text-muted">Optional; but if I have questions and I can't contact you, your submission might just get deleted. For privacy reasons, as soon as I approve a submission, the submitter's email is removed from the database.</small>
		</div>

		<div class="alert alert-warning">Your submission may be edited before being posted for accuracy or informativeness.</div>

		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Submit a new piece of media</button>
		
	    </form>
	</div>
	<div class="notes" id="correction">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>Suggest a correction</h2>
	    <p>Please indicate the piece of media to be corrected, what needs to be fixed, and provide a short justification for why.</p>
	    <form action="<?php echo SITE_URL; ?>correction.php" method="post">
		<div class="form-group">
		    <label for="media">What piece of media needs to be corrected?</label>
		    <select class="form-control" id="media" name="media">
			<option value="na"></option>
			<?php

			foreach ($rows as $media) {

			    if (! is_null ($media['details'])) {
				$details = " " . $media['details'];
			    } else {
				$details = "";
			    }
			    
			?><option value="<?php echo $media['id']; ?>"><?php echo $media['title']; ?> (<?php echo $media['year']; ?>)<?php echo $details; ?></option><?php
																				    }
																				    
																				    ?>
		    </select>
		    <small id="mediaHelp" class="form-text text-muted"></small>
		</div>


		<div class="form-group">
		    <label for="suggestion">Suggested correction</label>
		    <textarea class="form-control" id="suggestion" name="suggestion" rows="5"></textarea>
		    <small id="suggestionHelp" class="form-text text-muted">Please indicate what the error is and provide a brief explanation, if necessary.</small>
		</div>

		<div class="form-group">
		    <label for="submitter">Your email</label>
		    <input type="text" class="form-control" id="submitter" name="submitter" aria-describedby="submitterHelp">
		    <small id="submitterHelp" class="form-text text-muted">Optional; but if I have questions and I can't contact you, your suggested correction might just get deleted. For privacy reasons, all suggested corrections (including the submitter's email address) are deleted from the database automatically after 72 hours.</small>
		</div>

		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Suggest correction</button>
		
	    </form>
	</div>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <?php
		    echo $success_notice;
		    ?>
		    <h1>The Queerbait-Tragicqueer-Cishet-comfort Test</h1>
		    
		    <p style="font-style: italic;">Benjamin Gregory Carlisle PhD</p>
		    <img src="cc-by-nc.png" style="margin-bottom: 20px; margin-right: 2px;" title="This license lets others remix, adapt, and build upon this work non-commercially, and although their new works must also acknowledge Dr Carlisle and be non-commercial, they don’t have to license their derivative works on the same terms.">
		    <a class="btn btn-primary btn-sm" href="<?php echo SITE_URL; ?>export.tsv" style="margin-bottom: 20px;">Download this data set</a>
		    <a href="<?php echo SITE_URL; ?>feed/"><img src="rss.png" style="margin-bottom: 20px;" title="RSS feed"></a>
		    

		    <div class="jumbotron">
			<h1 class="display-4"><?php echo floor ( 100 * $full_marks_n / count ($rows) ); ?>% (<?php echo $full_marks_n; ?>/<?php echo count ($rows); ?>)</h1>
			<p class="lead">of the media reported here have at least one (1) named queer character who:</p>
			<hr class="my-4">
			<ul>
			    <li>Is clearly, unambiguously queer</li>
			    <li>Isn't a "blink-and-you-miss-it" queer</li>
			    <li>Isn't a tragic queer, and</li>
			    <li>Hasn't been written primarily to make cishet people comfortable</li>
			</ul>
			<hr class="my-4">
			<p>Please note that having "full marks" on this test does not mean that this media is necessarily an example of good queer representation. This test is intentionally a low bar.</p>
		    </div>

		    <button class="btn btn-primary" onclick="show_submit(event);" style="margin-bottom: 40px;">Submit a piece of media</button>
		    <button class="btn btn-secondary" onclick="show_correct(event);" style="margin-bottom: 40px;">Suggest a correction</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col-md-3">
		    <div class="input-group" style="margin-bottom: 40px;">
			<input type="text" id="filter-table-input" class="form-control" placeholder="Filter table" aria-label="Filter table" aria-describedby="filter-table-button" value="<?php echo $_GET['s']; ?>">
			<div class="input-group-append">
			    <button class="btn btn-primary" type="button" id="filter-table-button" onclick="event.preventDefault();filter_table();">Search</button>
			    
			</div>
		    </div>
		</div>
		<div class="col-md-3">
		    <button class="btn btn-success btn-block" onclick="toggle_non_fullmarks(event);" style="margin-bottom: 40px;">Hide media with less than "full marks"</button>
		</div>
		<div class="col-md-3">
		    <button class="btn btn-info btn-block" onclick="toggle_no_queer_characters(event);" style="margin-bottom: 40px;">Hide media with no queer characters</button>
		</div>
		<div class="col-md-3">
		    <button class="btn btn-secondary btn-block" type="button" onclick="$('#filter-table-input').val('');event.preventDefault();filter_table();" style="margin-bottom: 40px;">Show all</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <tr>
				<th scope="col">Media</th>
				<th scope="col">Queer character(s)<sup><a href="#queer_character_notes" onclick="highlight_paragraph('queer_character_notes');">1</a></sup></th>
				<th scope="col">Name<sup><a href="#name_notes" onclick="highlight_paragraph('name_notes');">2</a></sup> <button class="btn btn-warning btn-sm" onclick="unblur_all(event);">Unblur all</button></th>
				<th scope="col">Clearly queer<sup><a href="#clearly_queer" onclick="highlight_paragraph('clearly_queer');">3</a></sup></th>
				<th scope="col">Non-blink-and-you-miss-it<sup><a href="#blink_and_you_miss_it" onclick="highlight_paragraph('blink_and_you_miss_it');">4</a></sup></th>
				<th scope="col">Non-tragic<sup><a href="#tragic_notes" onclick="highlight_paragraph('tragic_notes');">5</a></sup></th>
				<th scope="col">Not written for cishet comfort<sup><a href="#cishet_comfort_notes" onclick="highlight_paragraph('cishet_comfort_notes');">6</a></sup></th>
				<th scope="col">Notes</th>
			    </tr>
			</thead>
			<tbody>
			    <?php

			    

			    foreach ($rows as $row) {

				$full_marks = FALSE;

				if ( $row['queer_character'] == 1 ) {
				    
				    $queer_character = '<td class="table-success">Yes</td>';

				    if ( $row['named'] == 1 ) {
					$name = '<td class="table-success name-column blurry-text">' . $row['name'] . '</td>';
				    } else {
					$name = '<td class="table-danger">-</td>';
				    }

				    if ( $row['non_deniable'] == 1 ) {
					$non_deniable = '<td class="table-success">Yes</td>';
				    } else {
					$non_deniable = '<td class="table-danger">No</td>';
				    }

				    if ( $row['blink_miss_it'] == 1 ) {
					$blink_miss_it = '<td class="table-danger">No</td>';
				    } else {
					$blink_miss_it = '<td class="table-success">Yes</td>';
				    }

				    if ( $row['tragic'] == 1 ) {
					$tragic = '<td class="table-danger">No</td>';
				    } else {
					if ( is_null ( $row['tragic'] ) ) {
					    $tragic = '<td class="table-warning">See notes</td>';
					} else {
					    $tragic = '<td class="table-success">Yes</td>';
					}
					
				    }

				    if ( $row['cishet_comfort'] == 1 ) {
					$cishet_comfort = '<td class="table-danger">No</td>';
				    } else {
					if ( is_null ( $row['cishet_comfort'] ) ) {
					    $cishet_comfort = '<td class="table-warning">See notes</td>';
					} else {
					    $cishet_comfort = '<td class="table-success">Yes</td>';
					}
					
				    }

				    // Calculate whether the piece of media gets "full marks"
				    
				    $show_row_class = TRUE;
				    
				    if ( $row['named'] == 1 & $row['non_deniable'] == 1 & $row['blink_miss_it'] == 0 & $row['tragic'] == 0 & $row['cishet_comfort'] == 0 ) {
					
					$row_class = ' class="fullmarks hasqueercharacter"';
					
				    } else {
					$row_class = ' class="hasqueercharacter"';
				    }
				    
				} else {
				    $queer_character = '<td class="table-danger">No</td>';
				    $name = '<td class="table-danger">-</td>';
				    $non_deniable = '<td class="table-danger">-</td>';
				    $blink_miss_it = '<td class="table-danger">-</td>';
				    $tragic = '<td class="table-danger">-</td>';
				    $cishet_comfort = '<td class="table-danger">-</td>';

				    $show_row_class = FALSE;
				}

			    ?><tr<?php if ($show_row_class) { echo $row_class; } ?>>
			    <th scope="row"><?php if ( ! is_null ( $row['imdb_link'] ) ) { ?><a href="<?php echo $row['imdb_link']; ?>" target="_blank"><?php } ?><?php echo $row['title']; ?><?php if ( ! is_null ( $row['year'] )) { ?> (<?php echo $row['year']; ?>)<?php } ?><?php if ( ! is_null ( $row['imdb_link'] ) ) { ?></a><?php } ?> <span class="badge badge-secondary"><?php echo $row['type']; ?></span><?php if ( ! is_null ( $row['details'] ) ) { ?><?php echo "<br>" . $row['details']; ?><?php } ?></th>
			    <?php echo $queer_character; ?>
			    <?php echo $name; ?>
			    <?php echo $non_deniable; ?>
			    <?php echo $blink_miss_it; ?>
			    <?php echo $tragic; ?>
			    <?php echo $cishet_comfort; ?>
			    <td><?php if ( ! is_null ( $row['notes'] ) ) { ?><button class="btn btn-primary btn-sm" onclick="show_notes(event, <?php echo $row['id']; ?>);">View</button><?php } else { echo "-"; } ?></td>
			    </tr><?php
				 
				 }
				 
				 ?>
			</tbody>
		    </table>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <h2>Details</h2>
		    <a name="queer_character_notes"></a>
		    <p id="queer_character_notes">1. Does not include fan-theories or details disclosed outside the media itself. Does not include campy or kinky cishet people. This only includes in-canon, non-metaphorical queer representation. For clarity: "allies" are not queer.</p>
		    <a name="name_notes"></a>
		    <p id="name_notes">2. The name of the queer character that passes or comes closest to passing all the requirements of The Queerbait-Tragicqueer-Cishet-comfort Test. In the case that there are many queer characters who would score the same, the first in alphabetical order. Because the name of a queer character may itself be a spoiler, they have been blurred. Mouse-over the cell in the table to reveal the name, or tap it on a mobile browser.</p>
		    <a name="clearly_queer"></a>
		    <p id ="clearly_queer">3. Would mis-reading this queer character as cishet be very difficult for even a naive cishet person, or one who is very determined to refuse to believe a character is queer? Of course it is impossible to write a queer character in such a way that the most determined cishet person couldn't refuse to acknowledge it.</p>
		    <a name="blink_and_you_miss_it"></a>
		    <p id="blink_and_you_miss_it">4. If the canonicity of a character's queerness depends entirely on a very small detail or a very short scene, it is blink-and-you-miss-it queer representation.</p>
		    <a name="tragic_notes"></a>
		    <p id="tragic_notes">5. Does the queer character in question meet with a sad ending, or is their story primarily about their misfortunes? This includes, but is not limited to "bury your gays" media, where the queer character is killed.</p>
		    <a name="cishet_comfort_notes"></a>
		    <p id="cishet_comfort_notes">6. Does this character seem to have been written mainly to coddle the sensibilities of cishet people, or to make the point that queer people are, or should be, indistinguishable from cishet people? <a href="https://blog.bgcarlisle.com/2020/04/15/risk-that-a-queer-character-was-written-for-straight-comfort-assessment-tool/" target="_blank">If unclear, use this tool.</a></p>
		</div>
	    </div>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- QT JS -->
	<script>
	 function show_citation (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#cite').slideDown();
	     });
	 }
	 
	 function show_notes (event, id) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#notes' + id).slideDown();
	     });
	 }

	 function hide_dialog (event) {
	     event.preventDefault();
	     $('.notes').slideUp(0, function () {
		 $('#citeMask').fadeOut();
	     });
	 }

	 function show_submit (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#submit').slideDown();
	     });
	 }

	 function show_correct (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#correction').slideDown();
	     });	     
	 }

	 function toggle_non_fullmarks (event) {
	     event.preventDefault();

	     if ($('#filter-table-input').val() != '') {
		 $('#filter-table-input').val('');
		 filter_table();
	     }
	     
	     $('tbody tr:not(.fullmarks)').fadeOut(0);
	 }

	 function toggle_no_queer_characters (event) {
	     event.preventDefault();

	     if ($('#filter-table-input').val() != '') {
		 $('#filter-table-input').val('');
		 filter_table();
	     }
	     
	     $('tbody tr:not(.hasqueercharacter)').fadeOut(0);
	 }

	 function unblur_all (event) {
	     event.preventDefault();
	     $('.name-column').toggleClass('blurry-text');
	 }

	 function filter_table () {

	     query = $('#filter-table-input').val();

	     if ( query == '' ) {
		 $('tbody th').parent().fadeIn(0);
	     } else {
		 $('tbody th').parent().fadeOut(0);

		 $('tbody th').each( function (index) {

		     cell_value = $(this).html();

		     if (cell_value.toLowerCase().search(query.toLowerCase()) != -1) {
			 $(this).parent().fadeIn(0);
		     }
		     
		 });
		 
	     }
	     

	 }

	 function highlight_paragraph (para_id) {

	     $('p').removeClass('paragraph-highlight');
	     
	     $('#' + para_id).addClass('paragraph-highlight');

	     setTimeout(function () {
		 $('#' + para_id).removeClass('paragraph-highlight');
	     }, 15000);
	 }

	 $('#filter-table-input').on('input', function (e) {
	     filter_table ();
	 });

	 $(document).ready(function () {
	     if ( $('#filter-table-input').val() != '' ) {
		 filter_table();
	     }

	     $('#queer_character').on('change', function () {
		 if ($(this).val() == "0") {
		     $('#queer_character_questions').slideUp();
		 } else {
		     $('#queer_character_questions').slideDown();
		 }
	     });
	 });

	</script>
    </body>
</html>

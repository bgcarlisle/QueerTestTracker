<?php

include_once ("../config.php");

if (admin_save_new_entry ($_POST['type'], $_POST['title'], $_POST['year'], $_POST['details'], $_POST['imdb_link'], $_POST['queer_character'], $_POST['named'], $_POST['name'], $_POST['non_deniable'], $_POST['blink_miss_it'], $_POST['tragic'], $_POST['cishet_comfort'], $_POST['notes'], $_POST['notes_contain_spoilers'])) {

    make_backup ();

    export_tsv ();
    
    $success_notice = '<div class="alert alert-success" role="alert">Successfully saved new entry</div>';
} else {
    $success_notice = '<div class="alert alert-danger" role="alert">Error in saving new entry</div>';
}

include ("index.php");

?>
